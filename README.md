gulp-move-to-directory-indexes
==============================

> A *gulp* plugin for moving a file into the index of a directory with the same basename.

## Setup and usage

Install `gulp-move-to-directory-index` using `npm`:

```sh
npm i gulp-move-to-directory-index
```

In your `gulpfile.js`:

```js
var gulp = require('gulp'),
    moveToDirectoryIndex = require('gulp-move-to-directory-index');

gulp.task('default', function() {
  return gulp.src('./src/**.*')
    .pipe(moveToDirectoryIndex())
    .pipe(gulp.dest('./dest'));
});
```

A common workflow using this tool is to move files so they can be directory indexes to hide the extensions in web servers like Apache.

## Options

### basename

*string*

Default: `index`

The name of the directory index file. The extension of the source file will be appended to the end of the basename.

### exclude

*RegExp*

Default: `/^\d{3}\.html$/`

The regular expression to exclude files from being processed. A common use is to exclude the HTTP status files (404.html) which don't need to be directory indexes.

### extensions

*string[]*

Default: `['.html']`

A list of extensions to move into their own directories. Files that don't match the given extension will be ignored.

### relativePaths

*string[]*

Default: []

A list of properties in the file that have a relative path in them. If they are present inside the file, they are mapped into a relative path.
