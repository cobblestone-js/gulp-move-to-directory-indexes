'use strict';

var expect = require('expect');
var plugin = require('../lib/index');
var File = require('vinyl');

describe('gulp-move-to-directory-index', function() {
    it('bob.html', function(done) {
        // Create a fake file that simulates `matter` being called on it.
        var fakeFile = new File({ path: 'test/bob.html' });

        // Pipe it through the plugin.
        var pipe = plugin({
        });

        pipe.write(fakeFile);

        // Verify the results of the parsing.
        pipe.once('data', function(file) {
            expect(file.path).toEqual('test/bob/index.html');
            done();
        });
    });

    it('bob/index.html', function(done) {
        // Create a fake file that simulates `matter` being called on it.
        var fakeFile = new File({ path: 'test/bob/index.html' });

        // Pipe it through the plugin.
        var pipe = plugin({
        });

        pipe.write(fakeFile);

        // Verify the results of the parsing.
        pipe.once('data', function(file) {
            expect(file.path).toEqual('test/bob/index.html');
            done();
        });
    });

    it('bob.text', function(done) {
        // Create a fake file that simulates `matter` being called on it.
        var fakeFile = new File({ path: 'test/bob.txt' });

        // Pipe it through the plugin.
        var pipe = plugin({
        });

        pipe.write(fakeFile);

        // Verify the results of the parsing.
        pipe.once('data', function(file) {
            expect(file.path).toEqual('test/bob.txt');
            done();
        });
    });

    it('404.html', function(done) {
        // Create a fake file that simulates `matter` being called on it.
        var fakeFile = new File({ path: 'test/404.html' });

        // Pipe it through the plugin.
        var pipe = plugin({
        });

        pipe.write(fakeFile);

        // Verify the results of the parsing.
        pipe.once('data', function(file) {
            expect(file.path).toEqual('test/404.html');
            done();
        });
    });

    it('bob.markdown', function(done) {
        // Create a fake file that simulates `matter` being called on it.
        var fakeFile = new File({ path: 'test/bob.markdown' });

        // Pipe it through the plugin.
        var pipe = plugin({
        });

        pipe.write(fakeFile);

        // Verify the results of the parsing.
        pipe.once('data', function(file) {
            expect(file.path).toEqual('test/bob.markdown');
            done();
        });
    });

    it('bob.markdown with markdown', function(done) {
        // Create a fake file that simulates `matter` being called on it.
        var fakeFile = new File({ path: 'test/bob.markdown' });

        // Pipe it through the plugin.
        var pipe = plugin({
            extensions: ['.html', '.markdown']
        });

        pipe.write(fakeFile);

        // Verify the results of the parsing.
        pipe.once('data', function(file) {
            expect(file.path).toEqual('test/bob/index.markdown');
            done();
        });
    });

    it('bob.html with images', function(done) {
        // Create a fake file that simulates `matter` being called on it.
        var fakeFile = new File({
            path: 'test/bob.html',
            rp: 'image.png'
        });

        // Pipe it through the plugin.
        var pipe = plugin({
            relativePaths: ['rp']
        });

        pipe.write(fakeFile);

        // Verify the results of the parsing.
        pipe.once('data', function(file) {
            expect(file.path).toEqual('test/bob/index.html');
            expect(file.rp).toEqual('../image.png');
            done();
        });
    });

    it('bob.html with missing image', function(done) {
        // Create a fake file that simulates `matter` being called on it.
        var fakeFile = new File({
            path: 'test/bob.html'
        });

        // Pipe it through the plugin.
        var pipe = plugin({
            relativePaths: ['rp']
        });

        pipe.write(fakeFile);

        // Verify the results of the parsing.
        pipe.once('data', function(file) {
            expect(file.path).toEqual('test/bob/index.html');
            expect(file.rp).toEqual(undefined);
            done();
        });
    });

    it('bob/index.html with images', function(done) {
        // Create a fake file that simulates `matter` being called on it.
        var fakeFile = new File({
            path: 'test/bob/index.html',
            rp: 'image.png'
        });

        // Pipe it through the plugin.
        var pipe = plugin({
            relativePaths: ['rp']
        });

        pipe.write(fakeFile);

        // Verify the results of the parsing.
        pipe.once('data', function(file) {
            expect(file.path).toEqual('test/bob/index.html');
            expect(file.rp).toEqual('image.png');
            done();
        });
    });

    it('bob/index.html with missing image', function(done) {
        // Create a fake file that simulates `matter` being called on it.
        var fakeFile = new File({
            path: 'test/bob/index.html'
        });

        // Pipe it through the plugin.
        var pipe = plugin({
            relativePaths: ['rp']
        });

        pipe.write(fakeFile);

        // Verify the results of the parsing.
        pipe.once('data', function(file) {
            expect(file.path).toEqual('test/bob/index.html');
            expect(file.rp).toEqual(undefined);
            done();
        });
    });
});
