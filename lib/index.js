var through = require('through2');
var path = require('path');
var dotted = require('dotted');

module.exports = function(options) {
    // Normalize the options.
    options = options || {};
    options.basename = options.basename || 'index';
    options.extensions = options.extensions || ['.html'];
    options.exclude = options.exclude || /^\d{3}\.html$/;
    options.relativePaths = options.relativePaths || [];

    // Set up the pipe that processes the file names.
    var setPipe = through.obj(
        function(file, encoding, callback) {
            // Check to see if we are processing a file with an extension we
            // are mapping. If we aren't, then we don't do anything new.
            var extension = path.extname(file.path);
            var isMovedExtension = options.extensions.indexOf(extension) >= 0;

            if (!isMovedExtension) {
                return callback(null, file);
            }

            // Check the base name to see if it was already moved. If it has,
            // then skip it.
            var basename = path.basename(file.path);

            if (basename === options.basename + extension) {
                return callback(null, file);
            }

            // See if the basename is excluded by the exclusion rules.
            if (options.exclude && basename.match(options.exclude)) {
                return callback(null, file);
            }

            // For everything else, we need to move to the index.
            var dirname = path.dirname(file.path);

            file.path = path.join(
                dirname,
                basename.replace(extension, ''),
                options.basename + extension);

            // If we have relative paths, we need to map them also.
            for (var index in options.relativePaths) {
                var relativePath = options.relativePaths[index];
                var relativeData = dotted.getNested(file, relativePath);

                if (relativeData) {
                    relativeData = '../' + relativeData;
                    dotted.setNested(file, relativePath, relativeData);
                }
            }

            // Finish up processing.
            return callback(null, file);
        });

    return setPipe;
};
